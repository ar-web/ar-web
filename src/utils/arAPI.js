import axios from './axiosInstanse.js'


export const arAPI = {};


arAPI.uploadFile = (file, onUploadProgress) => {
    let formData = new FormData();
    formData.append('file', file);
    return axios.post('/files', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        onUploadProgress: onUploadProgress || (progressEvent => console.log(progressEvent))
    })
}

arAPI.getFile = (name) => {
    return axios.get(`/files/${name}`)
}

arAPI.uploadModel = ({config, modelPath, name}) => {
    return axios.post('/models', {config, modelPath, name})
}

arAPI.getAllModels = () => {
    return axios.get('/models')
}

arAPI.getModel = (id) => {
    return axios.get(`/models/${id}`)
}

arAPI.getClientId = () => {
    return axios.get(`/auth`)
}


arAPI.addCustomModel = ({customConfig, modelId}) => {
    customConfig = JSON.stringify(customConfig);
    return axios.post(`/customs`, {customConfig, modelId})
}

arAPI.getCustomModel = (customId) => {
    return axios.get(`/customs/${customId}`)
}

arAPI.updateCustomModel = (customId, {customConfig, modelId, modelPath = 'string'}) => {
    customConfig = JSON.stringify(customConfig);
    return axios.put(`/customs/${customId}`, {customConfig, modelId, modelPath})
}
