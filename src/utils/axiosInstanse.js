import axios from "axios";
import {API_URL} from "./data";

// let token = localStorage.getItem('token');
let token = '';

const axiosInstance = axios.create({
    baseURL: API_URL,
    headers: token ? {'clientId': token} : {}
});

function updateAxios(token) {
    axiosInstance.defaults.headers['clientId'] = token;
}

export {
    updateAxios,
    axiosInstance
}

export default axiosInstance;