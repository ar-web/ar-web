import fs from 'fs';

const storageDir = 'static/modelsStorage';

if (!fs.existsSync(storageDir)){
    fs.mkdirSync(storageDir);
}

export function get(req, res, next) {
    console.log(fs.readdirSync(storageDir))

    res.writeHead(200, {
		'Content-Type': 'application/json'
	});

    res.end(
        JSON.stringify(
            fs.readdirSync(storageDir)
        )
    );
}

export function post(req, res, next) {
    console.log(req.body)
    res.end('ok')
}