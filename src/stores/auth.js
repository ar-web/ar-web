import { writable } from 'svelte/store';
import { updateAxios } from "../utils/axiosInstanse.js";

// let a = localStorage.getItem('ar-client-id');

// if (a) {
//     try {
//         updateAxios(a);
//     } catch {
//         a = null;
//     }
// } 

export const auth = writable('');

export const setAuth = (e) => {
    if (typeof e === 'string') {
        try {
            e = JSON.parse(e);
        } catch (err) {
            e = null;
        }
    }
    auth.set(e);
    if (e) {
        localStorage.setItem('ar-client-id', JSON.stringify(e));
        updateAxios(e.clientId);
    } else {
        localStorage.removeItem('ar-client-id');
    }
}